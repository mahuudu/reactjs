<?php
/**
 * The template for displaying the footer.
 *
 * @package flatsome
 */

global $flatsome_opt;
?>

</main>

<footer id="footer" class="footer-wrapper">

	<?php do_action('flatsome_footer'); ?>

</footer>

</div>


<?php wp_footer(); ?>

<script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/1.18.0/TweenMax.min.js"></script>

<script src="<?php echo THEME_URL_CHILD;?>/lib/slick/slick.min.js"></script>

<script src="<?php echo THEME_URL_CHILD;?>/lib/js/tkw.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>


<script>document.addEventListener( 'wpcf7mailsent', function( event ) {jQuery(".processing").removeClass("processing"); }, false ); document.addEventListener( 'wpcf7invalid', function( event ) {jQuery(".processing").removeClass("processing"); }, false );</script>
</html>
