jQuery(document).ready(function ($) {

	$(".language-toggle__btn").click(function () {
		$(".language-toggle").toggleClass("language-toggle--active");
	});

	var sgvActive = `<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 153 35.4" style="enable-background:new 0 0 153 35.4;" xml:space="preserve">
	<path class="st0" d="M151.9,11.7c0,0,3.3-9.5-85-8.3c-97,1.3-58.3,23-58.3,23s9.7,8.1,69.7,8.1c68.3,0,69.3-18.1,69.3-18.1 s1.7-7.5-14.7-15.4"></path>
	  </sgv>`;

	$('.product-categories li').append(sgvActive);

	$('.spr-summary-actions-newreview').click(function(){
		$('#review_form_wrapper').toggleClass("activeComment");
	});


	function slick() {
		// slick
		$('.related ul.products').slick({
			dots: false,
			infinite: true,
			slidesToShow: 3,
			autoplay: true,
			speed: 1000,
			slidesToScroll: 1,
			autoplaySpeed: 3000,
			centerMode: false,
			variableWidth: false,
			arrows: true,
			prevArrow: '<div class="slick-btn-slick-prev slick-btn"><i class="fas fa-angle-left" aria-hidden="true"></i></div>',
			nextArrow: '<div class="slick-btn-slick-next slick-btn"><i class="fas fa-angle-right" aria-hidden="true"></i></div>',
			responsive: [
			{
				breakpoint: 900,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1,
					infinite: true,
				}
			},
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
	
			]
		});
	
	}
	slick();

});

(function () { 
	for (var t = document.querySelectorAll(".category-icons__img"), n, e = 0; e < t.length; e++)
	 { 
		var r = t[e].querySelector(".category-icons__doodle");
	 	t[e].tl = new TimelineLite({ paused: !0 }), t[e].tl.from(r, 8, { rotation: 360, repeat: -1, ease: "linear" }), t[e].addEventListener("mouseenter", o),
	 	t[e].addEventListener("mouseleave", a) 
	}
	 function o() {
		this.tl || (t[e].tl = new TimelineLite({ paused: !0 })),
	    n !== this && this.tl.play(), n = this 
	} 
	function a() { n === this && this.tl.pause(), n = null } 
})();


function openCity(evt, cityName) {
	var i, x, tablinks;

	x = document.getElementsByClassName("city");
	for (i = 0; i < x.length; i++) {
	  x[i].style.display = "none";
	  x[i].classList.remove("tabbed-details__description--active");
	}
	tablinks = document.getElementsByClassName("tablink");
	for (i = 0; i < x.length; i++) {
	  tablinks[i].className = tablinks[i].className.replace(" tabbed-details__nav__item--active", "");
	}
	document.getElementById(cityName).style.display = "block";
	document.getElementById(cityName).classList.add("tabbed-details__description--active");
	evt.currentTarget.className += " tabbed-details__nav__item--active";
}

