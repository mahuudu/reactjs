(function () {
	var a = document.querySelector(".header-box"),
	x = document.querySelector(".header-box__canvas"),
	e = document.querySelector("canvas");
	e.width = a.clientWidth;
	e.height = a.clientHeight;



	var s = e.getContext("2d"),
	m = e.height,
	i = [],
	p = 10,
	d = 1,
	c = 6,
	r = 40,
	o = {
		x: 0,
		y: 0
	};
	function u() {
		s.clearRect(0, 0, e.width, e.height),
		s.beginPath();
		for (var t = 0; t < i.length; t++) i[t].position.x = Math.sin(i[t].count / i[t].distanceBetweenWaves) * 50 + i[t].xOff,
		i[t].position.y = i[t].count,
		i[t].render(),
		i[t].count < 0 - i[t].radius ? i[t].count = e.height + i[t].yOff: i[t].count -= d;
		for (var t = 0; t < i.length; t++) if (o.x > i[t].position.x - i[t].radius && o.x < i[t].position.x + i[t].radius && o.y > i[t].position.y - i[t].radius && o.y < i[t].position.y + i[t].radius) for (var h = 0; h < i[t].lines.length; h++) r = i[t].radius * .5,
		i[t].lines[h].popping = !0,
		i[t].popping = !0;
		window.requestAnimationFrame(u)
	}
	window.requestAnimationFrame(u);
	for (var f = function () {
		this.position = new Vec2(),
		this.radius = 8 + Math.random() * 6,
		this.xOff = Math.random() * e.width - this.radius,
		this.yOff = Math.random() * e.height,
		this.distanceBetweenWaves = 50 + Math.random() * 40,
		this.count = e.height + this.yOff,
		this.color = "#8bc9ee",
		this.lines = [],
		this.popping = !1,
		this.maxRotation = 85,
		this.rotation = Math.floor(Math.random() * (this.maxRotation - this.maxRotation * -1)) + this.maxRotation * -1,
		this.rotationDirection = "forward",
		this.img = new Image,
		this.img.src = "https://cdn.shopify.com/s/files/1/1542/1311/t/55/assets/grip__bubble.svg?v=799591955096842326";
		for (var t = 0; t < c; t++) {
			var h = new g;
			h.bubble = this,
			h.index = t,
			this.lines.push(h)
		}
		this.resetPosition = function () {
			this.position = new Vec2(),
			this.radius = 8 + Math.random() * 6,
			this.xOff = Math.random() * e.width - this.radius,
			this.yOff = Math.random() * e.height,
			this.distanceBetweenWaves = 50 + Math.random() * 40,
			this.count = e.height + this.yOff,
			this.popping = !1
		},
		this.render = function () {
			this.rotationDirection === "forward" ? this.rotation < this.maxRotation ? this.rotation++:this.rotationDirection = "backward": this.rotation > this.maxRotation * -1 ? this.rotation--:this.rotationDirection = "forward",
			this.popping || (s.save(), s.translate(this.position.x, this.position.y), s.rotate(this.rotation * Math.PI / 180), s.drawImage(this.img, this.radius * -1, this.radius * -1, this.radius * 2, this.radius * 2), s.restore()),
			s.beginPath(),
			s.strokeStyle = "rgba(0,0,0, 0)",
			s.save(),
			s.translate(this.position.x, this.position.y),
			s.rotate(this.rotation * Math.PI / 180),
			s.arc(0, 0, this.radius, 0, Math.PI * 2, !1),
			s.restore(),
			s.stroke();
			for (var n = 0; n < this.lines.length; n++) this.lines[n].popping && (this.lines[n].lineLength < r && !this.lines[n].inversePop ? this.lines[n].popDistance += .06 : this.lines[n].popDistance >= 0 ? (this.lines[n].inversePop = !0, this.lines[n].popDistanceReturn += 1, this.lines[n].popDistance -= .03) : (this.lines[n].resetValues(), this.resetPosition()), this.lines[n].updateValues(), this.lines[n].render())
		}
	},
	l = 0; l < p; l++) {
		var b = new f;
		i.push(b)
	}
	function g() {
		this.lineLength = 0,
		this.popDistance = 0,
		this.popDistanceReturn = 0,
		this.inversePop = !1,
		this.popping = !1,
		this.resetValues = function () {
			this.lineLength = 0,
			this.popDistance = 0,
			this.popDistanceReturn = 0,
			this.inversePop = !1,
			this.popping = !1,
			this.updateValues()
		},
		this.updateValues = function () {
			this.x = this.bubble.position.x + (this.bubble.radius + this.popDistanceReturn) * Math.cos(2 * Math.PI * this.index / this.bubble.lines.length),
			this.y = this.bubble.position.y + (this.bubble.radius + this.popDistanceReturn) * Math.sin(2 * Math.PI * this.index / this.bubble.lines.length),
			this.lineLength = this.bubble.radius * this.popDistance,
			this.endX = this.lineLength,
			this.endY = this.lineLength
		},
		this.render = function () {
			this.updateValues(),
			s.beginPath(),
			s.strokeStyle = "#8bc9ee",
			s.lineWidth = 2,
			s.moveTo(this.x, this.y),
			this.x < this.bubble.position.x && (this.endX = this.lineLength * -1),
			this.y < this.bubble.position.y && (this.endY = this.lineLength * -1),
			this.y === this.bubble.position.y && (this.endY = 0),
			this.x === this.bubble.position.x && (this.endX = 0),
			s.lineTo(this.x + this.endX, this.y + this.endY),
			s.stroke()
		}
	}
	e.addEventListener("mousemove", v);
	function v(t) {
		o.x = t.offsetX,
		o.y = t.offsetY
	}
	window.addEventListener("resize", function () {
		e.width = a.clientWidth,
		e.height = a.clientHeight
	})
})();