jQuery(document).ready(function ($) {
    $('.archive .product-listing select').on('change', function() {
        var id = $(this).attr("id");
        var idProductDom = `#product_${id}`;
        let observer;
        var page = $(`${idProductDom}`)[0];
        
        var config = { characterData: true,
            attributes: false,
            childList: true,
            subtree: true 
        };
        
        var callback = function( mutationsList, observer ){

            var price = $(`${idProductDom} .woocommerce-Price-amount`);
            
            if (price) {
                let priceText = $(`${idProductDom} .woocommerce-Price-amount`).text();
                let btnAdd = $(`#btn_add_${id}`).text(`ADD TO BAG ${priceText}`);
                observer.disconnect();
            }
        }

        observer = new MutationObserver( callback );
        observer.observe( page, config );
        
      });


      $('#online_filter').click(function(){
        if($(this).is(":checked")){
            $('.outstock').css('display','none');
        }
        else if($(this).is(":not(:checked)")){
            $('.outstock').css('display','block');
        }
    });

});

