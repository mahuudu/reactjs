<?php
/*
Template name: Page - Full Width - Ingredient
*/
get_header(); ?>

<div id="content" role="main">

	<?php while ( have_posts() ) : the_post(); ?>

		<?php the_content(); ?>
	
	<?php endwhile; // end of the loop. ?>
			
</div>
<script src="<?php echo THEME_URL_CHILD;?>/lib/js/grip__ingredients.js"></script>

<?php get_footer(); ?>