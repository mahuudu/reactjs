<?php
$args = array(
    'numberposts'    => -1,
    'showposts'    => 10,
    'post_type'        => 'product',
    'post_status'       =>  'publish',
    'meta_query'    => array(
        'relation'        => 'OR',
        array(
            'key'        => 'address',
            'value'        => $key,
            'compare'    => 'LIKE'
        )
    )


); ?>
<div class="top-container">
    <div class="section-content relative">

        <div class="row" id="row-1643223279">

            <div id="col-167552744" class="col small-12 large-12">
                <div class="col-inner">
                    <div class="container section-title-container heading-1 " style="margin-top: 25px;">
                        <h3 class="section-title section-title-normal"><b></b><span class="section-title-main" style="color:rgb(0, 175, 233);">Where to buy search</span><b></b></h3>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="row">
    <div class="col-12 col-md-6 col-lg-4">
    <h3 class="section-title section-title-normal"><b></b><span class="section-title-main" style="color:rgb(0, 175, 233);">Find store </span><b></b></h3>
        <div class="search-address">
          <form action="<?php echo esc_url(home_url('/')); ?>" method="GET" role="form" class="d-flex">
            <input type="hidden" name="post_type" value="address">
            <input type="text" name="s" id="s-home" autocomplete="off" placeholder="Address....">
            <button type="submit" class="button-search btn-4"><i class="fa fa-search"></i></button>
          </form>
        </div>
    </div>
    <div class="col-12 col-lg-8">
        <div class="content-home-slick">
            <div class="row">
                <?php $the_query = new WP_Query($args); ?>
                <?php if ($the_query->have_posts()) : ?>
                    <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                        <div class="img-add">
                            <?php
                            if (function_exists('get_field')) {
                                $images = get_field('address_image', get_the_ID());
                            ?>
                                <?php
                                if ($images) : ?>
                                    <?php foreach ($images as $image) : ?>
                                        <div class="logo-listing__item">
                                            <a href="<?php echo esc_url($image['url']); ?>"  class="ps-gallery">
                                                <img src="<?php echo esc_url($image['url']); ?>"  />
                                            </a>
                                        </div>
                                    <?php endforeach; ?>

                                <?php endif; ?>
                            <?php } ?>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
                <?php wp_reset_query(); ?>
            </div>
        </div>
    </div>
</div>