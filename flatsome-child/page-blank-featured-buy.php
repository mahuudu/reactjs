<?php
/*
Template name: Page - Where to buy
*/
get_header(); ?>

<div id="content" role="main">
  <?php while (have_posts()) : the_post(); ?>
    <?php the_content(); ?>
    <div class="row">
      <div class="col-lg-4 col-md-12 col-12">
      <h3 class="section-title section-title-normal"><b></b><span class="section-title-main" style="color:rgb(0, 175, 233);">Find store </span><b></b></h3>
        <div class="search-address">
          <form action="<?php echo esc_url(home_url('/')); ?>" method="GET" role="form" class="d-flex">
            <input type="hidden" name="post_type" value="address">
            <input type="text" name="s" id="s-home" autocomplete="off" placeholder="Address....">
            <button type="submit" class="button-search btn-4"><i class="fa fa-search"></i></button>
          </form>
        </div>
      </div>
      <div class="col-lg-8 col-md-12 col-12">
      <div class="mapouter"><div class="gmap_canvas"><iframe width="600" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=Ha%20Noi&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://putlocker-is.org"></a><br><style>.mapouter{position:relative;text-align:right;height:500px;width:600px;}</style><a href="https://www.embedgooglemap.net">google maps code</a><style>.gmap_canvas {overflow:hidden;background:none!important;height:500px;width:600px;}</style></div></div>
      </div>
    </div>
  <?php endwhile; 

  ?>

</div>

<?php get_footer(); ?>
