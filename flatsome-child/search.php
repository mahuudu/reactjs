<?php get_header(); ?>

<div id="content">
	<div id="content-container" class="container pd-top-40">
		<main id="main">			
			<?php    
			$key 		= isset($_GET['s']) && $_GET['s'] ? $_GET['s'] : '';
			$post_type  = isset($_GET['post_type']) && $_GET['post_type'] ? $_GET['post_type'] : 'post';

			if ($post_type == "address") {
				$taxonomy = 'product';
					include_once( get_stylesheet_directory() .'/parts/searchResult/search-product.php');
				?>
			<?php  }    ?>
		</main>
	</div>
</div>

<?php
/**
 * The template for displaying the footer.
 *
 * @package flatsome
 */

global $flatsome_opt;
?>

</main>


</div>

<footer id="footer" class="footer-wrapper">

	<?php do_action('flatsome_footer'); ?>

</footer>

</div>

<?php wp_footer(); ?>

</body>
</html>


