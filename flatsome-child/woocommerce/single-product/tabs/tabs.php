<?php

/**
 * Single Product tabs
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/tabs/tabs.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 2.4.0
 */

if (!defined('ABSPATH')) {
	exit;
}

/**
 * Filter tabs and allow third parties to add their own.
 *
 * Each tab is an array containing title, callback and priority.
 * @see woocommerce_default_product_tabs()
 */
$tabs = apply_filters('woocommerce_product_tabs', array());

if (!empty($tabs)) : ?>

	<div class="tabbed-details">

		<div class="tabbed-details__nav">
			<button class="tabbed-details__nav__item tablink tabbed-details__nav__item--active" onclick="openCity(event,'a')">Ingredients</button>
			<button class="tabbed-details__nav__item tablink" onclick="openCity(event,'b')">Certificates</button>
			<button class="tabbed-details__nav__item tablink" onclick="openCity(event,'c')">Reviews</button>
			<button class="tabbed-details__nav__item tablink" onclick="openCity(event,'d')">Badges</button>
		</div>

		<div id="a" class="tabbed-details__description tabbed-details__description--active  city">
			<?php the_content(); ?>
		</div>

		<div id="b" class="tabbed-details__description  city" style="display:none">
			<div class="logo-listing logo-listing--flex-start text-align-center">
				<?php
				if (function_exists('get_field')) {
					$images = get_field('certificates', get_the_ID());
				?>
					<?php
					if ($images) : ?>
						<?php foreach ($images as $image) : ?>
							<div class="logo-listing__item">
								<a href="<?php echo esc_url($image['url']); ?>" data-thumb="<?php echo esc_url($image['url']); ?>" data-src="<?php echo esc_url($image['url']); ?>" class="ps-gallery">
									<img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
								</a>
							</div>
						<?php endforeach; ?>

					<?php endif; ?>
				<?php } ?>
			</div>
		</div>

		<div id="c" class="tabbed-details__description city" style="display:none">
			<?php  echo comments_template(); ?>
		</div>

		<div id="d" class="tabbed-details__description city" style="display:none">
			<?php
			if (function_exists('get_field')) {
				$badges = get_field('badges', get_the_ID());
			?>
				<?php
				if ($badges) : ?>
					<?php echo $badges; ?>
				<?php endif; ?>
			<?php } ?>
		</div>

	</div>

<?php endif; ?>