<?php

/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined('ABSPATH') || exit;

global $product;

// Ensure visibility.
if (fl_woocommerce_version_check('4.4.0')) {
	if (empty($product) || false === wc_get_loop_product_visibility($product->get_id()) || !$product->is_visible()) {
		return;
	}
} else {
	if (empty($product) || !$product->is_visible()) {
		return;
	}
}

// Check stock status.
$out_of_stock = !$product->is_in_stock();

$columns = esc_attr(wc_get_loop_prop('columns'));
$current_post = $wp_query->current_post;

// Extra post classes.
$classes   = array();
$classes[] = 'product-small';
$classes[] = 'col';
$classes[] = 'has-hover';

if ($out_of_stock) $classes[] = 'out-of-stock';

?>
<?php
if ($current_post == 4 ) :
if(function_exists('get_field')){
$term = get_queried_object();

?>
	<div id="<?php echo 'product_' . $product->get_id(); ?>" class="product-listing__item">
		<div class="product-callout">
			<div class="product-callout__title">did you know?</div>
			<div class="product-callout__description">
				<?php
					$value = get_field( "category_know",  $term );

					if( $value ) {
						echo $value;
					} else {
						echo '';
					}

				?>
			</div>
		</div>
	</div>
<?php
}
endif;
?>

<div id="<?php echo 'product_' . $product->get_id(); ?>" class="product-listing__item <?php if ( $out_of_stock ) { echo "outstock"; } ?>">

	<a href="<?php the_permalink(); ?>" class="order-now-overlay ">
		<?php the_post_thumbnail("thumbnail", array("title" => get_the_title(), "alt" => get_the_title(), "class" => 'product-listing__item__img')); ?>
		<span class="product-listing__item__title"> <?php
													the_title();
													?>
		</span>
	</a>
	<?php
	if ($product->is_type('variable')) {

		wp_enqueue_script('wc-add-to-cart-variation');

		$attribute_keys = array_keys($product->get_attributes());
	?>

		<form class="variations_form cart" method="post" enctype='multipart/form-data' data-product_id="<?php echo absint($product->id); ?>" data-product_variations="<?php echo htmlspecialchars(json_encode($product->get_available_variations())) ?>">
			<?php do_action('woocommerce_before_variations_form'); ?>

			<?php if (empty($product->get_available_variations()) && false !== $product->get_available_variations()) : ?>
				<p class="stock out-of-stock"><?php _e('This product is currently out of stock and unavailable.', 'woocommerce'); ?></p>
			<?php else : ?>
				<table class="variations" cellspacing="0">
					<tbody>
						<?php foreach ($product->get_variation_attributes() as $attribute_name => $options) : ?>
							<tr>
								<td class="value">
									<?php
									$selected = isset($_REQUEST['attribute_' . sanitize_title($attribute_name)]) ? wc_clean(urldecode($_REQUEST['attribute_' . sanitize_title($attribute_name)])) : $product->get_variation_default_attribute($attribute_name);
									wc_dropdown_variation_attribute_options(array('options' => $options, 'attribute' => $attribute_name, 'product' => $product, 'selected' => $selected, 'id' => $product->get_id()));
									echo end($attribute_keys) === $attribute_name ? apply_filters('woocommerce_reset_variations_link', '<a class="reset_variations" href="#">' . __('Clear', 'woocommerce') . '</a>') : '';
									?>
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>

				<?php do_action('woocommerce_before_add_to_cart_button'); ?>

				<div class="single_variation_wrap">
					<?php
					/**
					 * woocommerce_before_single_variation Hook.
					 */
					do_action('woocommerce_before_single_variation');

					/**
					 * woocommerce_single_variation hook. Used to output the cart button and placeholder for variation data.
					 * @since 2.4.0
					 * @hooked woocommerce_single_variation - 10 Empty div for variation data.
					 * @hooked woocommerce_single_variation_add_to_cart_button - 20 Qty and cart button.
					 */
					do_action('woocommerce_single_variation');

					/**
					 * woocommerce_after_single_variation Hook.
					 */
					do_action('woocommerce_after_single_variation');
					?>
				</div>

				<?php do_action('woocommerce_after_add_to_cart_button'); ?>
			<?php endif; ?>

			<?php do_action('woocommerce_after_variations_form'); ?>
		</form>

	<?php } else { ?>
		<div class="btn-no-vari">
			<?php echo sprintf(
				'<a rel="nofollow" href="%s" data-quantity="%s" data-product_id="%s" data-product_sku="%s" class="%s">%s</a>',
				esc_url($product->add_to_cart_url()),
				esc_attr(isset($quantity) ? $quantity : 1),
				esc_attr($product->id),
				esc_attr($product->get_sku()),
				esc_attr(isset($class) ? $class : 'button'),
				esc_html("ADD TO BAG " . $product->get_price() . "$")
			); ?>
		</div>
	<?php }
	?>


</div>