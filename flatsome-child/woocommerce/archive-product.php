<?php

/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined('ABSPATH') || exit;

get_header('shop');
global $wp_query;
$cat = $wp_query->get_queried_object();
$thumbnail_id = get_term_meta($cat->term_id, 'thumbnail_id', true);
$image = wp_get_attachment_url($thumbnail_id);
?>
<div class="container main-content">
	<div class="row">
		<div class="col-md-3 col-lg-3 col-sm-12 col-12 isDesktop">
			<?php
			if (is_active_sidebar('product-sidebar')) {
				dynamic_sidebar('product-sidebar');
			} else if (is_active_sidebar('shop-sidebar')) {
				dynamic_sidebar('shop-sidebar');
			}
			?>
		</div>
		<div class="col-md-9 col-lg-9 col-sm-12 col-12">
			<div class="main-woo">
				<?php
				if ($image) { ?>
					<div class="tout-rotator">
						<div class="category_tout-rotator__touts">
							<div class="category-tout-rotator__touts__item">
								<img class="category_tout_image" src="<?php echo $image ?>" alt="<?php echo $cat->name ?>">
								<?php if (apply_filters('woocommerce_show_page_title', true)) : ?>
									<h1 class="tout-rotator__touts__item__tagline tout-online-store"><?php woocommerce_page_title(); ?></h1>
								<?php endif; ?>
							</div>
						</div>
					</div>
				<?php } else { ?>
					<div class="header-box">
						<canvas class="header-box__canvas" width="733" height="230"></canvas>
						<?php if (apply_filters('woocommerce_show_page_title', true)) : ?>
							<h1 class="header-box__title"><?php woocommerce_page_title(); ?></h1>
						<?php endif; ?>
						<div class="header-box__description">
							<?php
							/**
							 * Hook: woocommerce_archive_description.
							 *
							 * @hooked woocommerce_taxonomy_archive_description - 10
							 * @hooked woocommerce_product_archive_description - 10
							 */
							do_action('woocommerce_archive_description');
							?>
						</div>
					</div>
				<?php } ?>
				<div class="product-filter" style="padding-top: 20px; padding-bottom: 20px; width: 100%; text-align: center;">
					<input class="filter_checkbox" type="checkbox" id="online_filter" name="online_filter">
					<label class="filter_checkbox_label" for="online_filter"></label>
					<div class="new_file_checkbox_label">Available for Online Purchase</div>
				</div>
				<?php
				if (woocommerce_product_loop()) {

					/**
					 * Hook: woocommerce_before_shop_loop.
					 *
					 * @hooked woocommerce_output_all_notices - 10
					 * @hooked woocommerce_result_count - 20
					 * @hooked woocommerce_catalog_ordering - 30
					 */
					do_action('woocommerce_before_shop_loop'); ?>
					<div class="product-listing">
						<?php woocommerce_product_loop_start();
						if (wc_get_loop_prop('total')) {
							while (have_posts()) {
								the_post();

								/**
								 * Hook: woocommerce_shop_loop.
								 */
								do_action('woocommerce_shop_loop');

								wc_get_template_part('content', 'product');
							}
						}

						woocommerce_product_loop_end();

						/**
						 * Hook: woocommerce_after_shop_loop.
						 *
						 * @hooked woocommerce_pagination - 10
						 */
						do_action('woocommerce_after_shop_loop'); ?>
					</div>

				<?php } else {
					/**
					 * Hook: woocommerce_no_products_found.
					 *
					 * @hooked wc_no_products_found - 10
					 */
					do_action('woocommerce_no_products_found');
				}

				/**
				 * Hook: woocommerce_after_main_content.
				 *
				 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
				 */
				do_action('woocommerce_after_main_content');

				?>
			</div>
		</div>

	</div>
</div>

<script src="<?php echo THEME_URL_CHILD; ?>/lib/js/vec2.min.js"></script>

<script src="<?php echo THEME_URL_CHILD; ?>/lib/js/grip__header-box-canvas.js"></script>

<script src="<?php echo THEME_URL_CHILD; ?>/lib/js/product.js"></script>
<?php
get_footer('shop');
