<?php
// Add custom Theme Functions here
define('THEME_URL', get_template_directory_uri());

define('THEME_URL_CHILD', get_stylesheet_directory_uri());


// Add Shortcode [cart_count]
function get_cart_count()
{

    if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {
        global $woocommerce;
        echo "<div class=" . 'cart-count' . ">";
        echo $woocommerce->cart->cart_contents_count;
        echo "</div>";
    }
}
add_shortcode('cart_count', 'get_cart_count');

add_action('init', 'my_remove_lightbox');
function my_remove_lightbox()
{
    remove_theme_support('wc-product-gallery-lightbox');
}

remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
add_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 25);

add_filter('woocommerce_dropdown_variation_attribute_options_html', 'filter_dropdown_option_html', 12, 2);
function filter_dropdown_option_html($html, $args)
{
    $show_option_none_text = $args['show_option_none'] ? $args['show_option_none'] : __('Choose an option', 'woocommerce');
    $show_option_none_text = esc_html($show_option_none_text);

    $html = str_replace($show_option_none_text, 'Select', $html);

    return $html;
}

add_filter('woocommerce_quantity_input_min', 'hide_woocommerce_quantity_input', 10, 2);
add_filter('woocommerce_quantity_input_max', 'hide_woocommerce_quantity_input', 10, 2);

function hide_woocommerce_quantity_input($quantity, $product)
{
    // only on the product page
    if (!is_product()) {
        return $quantity;
    }
    return 1;
}

remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50);

add_action('woocommerce_after_shop_loop_item', 'get_star_rating');
function get_star_rating()
{
    global $woocommerce, $product;
    $average = $product->get_average_rating();

    echo '<div class="star-rating"><span style="width:' . (($average / 5) * 100) . '%"><strong itemprop="ratingValue" class="rating">' . $average . '</strong> ' . __('out of 5', 'woocommerce') . '</span></div>';
}

remove_filter('woocommerce_show_page_title', 'filter_woocommerce_show_page_title', 10, 2);


// For Woocommerce version 3 and above only
add_filter('woocommerce_loop_add_to_cart_link', 'filter_loop_add_to_cart_link', 20, 3);
function filter_loop_add_to_cart_link($button, $product, $args = array())
{
    if ($product->is_in_stock()) return $button;

    // HERE set your button text (when product is not on stock)
    $button_text = __('Not available', 'woocommerce');

    // HERE set your button STYLING (when product is not on stock)
    $color = "#777";      // Button text color
    $background = "#aaa"; // Button background color

    // Changing and disbling the button when products are not in stock
    $style = 'color:' . $color . ';background-color:' . $background . ';cursor:not-allowed;';
    return sprintf('<a class="button disabled" style="%s">%s</a>', $style, $button_text);
}


// Dumh create tab panal
function devvn_ux_builder_element()
{
    add_ux_builder_shortcode('devvn_viewnumber', array(
        'name'      => __('Id ẩn'),
        'category'  => __('Content'),
        'priority'  => 1,
        'options' => array(
            'text1'    =>  array(
                'type' => 'textfield',
                'heading' => 'Id ẩn to scrooll',
                'default' => '',
            ),
        ),
    ));
}
add_action('ux_builder_setup', 'devvn_ux_builder_element');

function devvn_viewnumber_func($atts)
{
    extract(shortcode_atts(array(
        'text1'    => '',
 
    ), $atts));
    ob_start(); ?>
    <?php $string = str_replace(' ', '', $text1);?>
    <div id="<?php echo $string; ?>">

    </div>

<?php return ob_get_clean();
}
add_shortcode('devvn_viewnumber', 'devvn_viewnumber_func');


//strong man
function devvn_ux_builder_element_1()
{
    add_ux_builder_shortcode('devvn_viewnumber_1', array(
        'name'      => __('Ingredient Strongman'),
        'category'  => __('Content'),
        'priority'  => 1,
        'options' => array(
            'checka' => array(
                'type' => 'radio-buttons',
                'heading' => __('Check a'),
                'default' => 'false',
                'options' => array(
                    'false'  => array( 'title' => 'Off'),
                    'true'  => array( 'title' => 'On'),
                ),
            ),
            'checkb' => array(
                'type' => 'radio-buttons',
                'heading' => __('Check b'),
                'default' => 'false',
                'options' => array(
                    'false'  => array( 'title' => 'Off'),
                    'true'  => array( 'title' => 'On'),
                ),
            ),
            'checkc' => array(
                'type' => 'radio-buttons',
                'heading' => __('Check c'),
                'default' => 'false',
                'options' => array(
                    'false'  => array( 'title' => 'Off'),
                    'true'  => array( 'title' => 'On'),
                ),
            ),
            'checkd' => array(
                'type' => 'radio-buttons',
                'heading' => __('Check d'),
                'default' => 'false',
                'options' => array(
                    'false'  => array( 'title' => 'Off'),
                    'true'  => array( 'title' => 'On'),
                ),
            ),
            'checke' => array(
                'type' => 'radio-buttons',
                'heading' => __('Check e'),
                'default' => 'false',
                'options' => array(
                    'false'  => array( 'title' => 'Off'),
                    'true'  => array( 'title' => 'On'),
                ),
            ),
            'checkf' => array(
                'type' => 'radio-buttons',
                'heading' => __('Check f'),
                'default' => 'false',
                'options' => array(
                    'false'  => array( 'title' => 'Off'),
                    'true'  => array( 'title' => 'On'),
                ),
            ),
     
        ),
    ));
}
add_action('ux_builder_setup', 'devvn_ux_builder_element_1');

function devvn_viewnumber_func_1($atts)
{
    extract(shortcode_atts(array(
        'checka'    => 'false',
        'checkb'    => 'false',
        'checkc'    => 'false',
        'checkd'    => 'false',
        'checke'    => 'false',
        'checkf'    => 'false',
    ), $atts));
    ob_start(); ?>

    <div class="sponge-man">
        <img src="<?php echo THEME_URL_CHILD;?>/images/grip__sponge-man.png" alt="">
        <?php if($checka == 'true'){?>
            <button type="button" name="button" class="sponge-man__btn clear--button-styles sponge-man__btn--a" data-letter="a">
              <img src="<?php echo THEME_URL_CHILD;?>/images/grip__a@2x.png" alt="" style="width: 30px">
             </button>
        <?php }?>
        <?php if($checkb == 'true'){?>
            <button type="button" name="button" class="sponge-man__btn clear--button-styles sponge-man__btn--b" data-letter="b">
            <img src="<?php echo THEME_URL_CHILD;?>/images/grip__b@2x.png" alt="" style="width: 34px">
            </button>
        <?php }?>
        <?php if($checkc == 'true'){?>
            <button type="button" name="button" class="sponge-man__btn clear--button-styles sponge-man__btn--c" data-letter="c">
            <img src="<?php echo THEME_URL_CHILD;?>/images/grip__c@2x.png" alt="" style="width: 32px">
            </button>
        <?php }?>
        <?php if($checkd == 'true'){?>
            <button type="button" name="button" class="sponge-man__btn clear--button-styles sponge-man__btn--d" data-letter="d">
               <img src="<?php echo THEME_URL_CHILD;?>/images/grip__d@2x.png" alt="" style="width: 30px">
           </button>
        <?php }?>
        <?php if($checke == 'true'){?>
            <button type="button" name="button" class="sponge-man__btn clear--button-styles sponge-man__btn--e" data-letter="e">
               <img src="<?php echo THEME_URL_CHILD;?>/images/grip__e@2x.png" alt="" style="width: 32px">
          </button>
        <?php }?>   
        <?php if($checkf == 'true'){?>
            <button type="button" name="button" class="sponge-man__btn clear--button-styles sponge-man__btn--f" data-letter="f">
               <img src="<?php echo THEME_URL_CHILD;?>/images/grip__f@2x.png" alt="" style="width: 32px">
          </button>
        <?php }?> 
    
    </div>

<?php return ob_get_clean();
}
add_shortcode('devvn_viewnumber_1', 'devvn_viewnumber_func_1');



